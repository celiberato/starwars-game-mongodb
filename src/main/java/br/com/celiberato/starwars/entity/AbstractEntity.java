package br.com.celiberato.starwars.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractEntity {

	private static final long serialVersionUID = 1L;

}
