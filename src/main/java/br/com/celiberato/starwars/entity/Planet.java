package br.com.celiberato.starwars.entity;

import org.springframework.data.annotation.Id;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@FieldDefaults(level = AccessLevel.PROTECTED)
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Planet extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	@Id
	protected String id;
	protected String name;
	protected String climate;
	protected String terrain;
	protected Long quantity;

	
}
