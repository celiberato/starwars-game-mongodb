package br.com.celiberato.starwars.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.celiberato.starwars.response.PlanetResponse;
import feign.Headers;

@Headers("Accept: application/json")
@FeignClient(url = "https://swapi.dev/api/", name = "StarWars")
public interface StarWarsClient {

	@GetMapping(path = "planets?page={page}", consumes = MediaType.APPLICATION_JSON_VALUE)
	@Headers("Content-Type: application/json")
	PlanetResponse findAllPlanets(@PathVariable("page") int page);
	
}
