package br.com.celiberato.starwars.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;
import br.com.celiberato.starwars.exception.BusinessException;
import br.com.celiberato.starwars.service.PlanetDBService;
import br.com.celiberato.starwars.util.EntityDtoUtil;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/db/planets")
public class PlanetDBController extends AbstractController {

    @Autowired
    private PlanetDBService service;

    @Transient
    private PlanetDto planetObj;
    
    @GetMapping("all")
    public Flux<PlanetDto> all(){
        return prepareFlux(this.service.getAll());
    }

    @GetMapping("/name/{name}")
    public Flux<PlanetDto> getByName(@PathVariable("name") String name){
    	return prepareFlux(this.service.getPlanetByName(name));
    }

    @GetMapping("{id}")
    public Mono<PlanetDto> getPlanetById(@PathVariable String id){
        return prepareMono(this.service.getPlanetById(id));
    }

    @PostMapping("/create")
    public Mono<PlanetDto> createPlanet(@RequestBody PlanetDto planetDto) throws BusinessException{
    	return prepareMono(service.createPlanet(convertToDTO(planetDto, Planet.class)));
    }
    
    
    @PutMapping("{id}")
    public Mono<PlanetDto> updatePlanet(@PathVariable String id, @RequestBody PlanetDto planetDto){
       return prepareMono(this.service.updatePlanet(id, planetDto));
    }

    @DeleteMapping("{id}")
    public void deleteProduct(@PathVariable String id) throws BusinessException {
    	this.service.deletePlanet(id);
    }
}
