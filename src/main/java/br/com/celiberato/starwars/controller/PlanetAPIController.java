package br.com.celiberato.starwars.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.celiberato.starwars.dto.PlanetClientDTO;
import br.com.celiberato.starwars.response.PlanetResponse;
import br.com.celiberato.starwars.service.PlanetAPIService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;


@RestController
@RequestMapping(value = "/api/planets", produces = { MediaType.APPLICATION_JSON_VALUE})
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlanetAPIController extends AbstractController {

	@Autowired
	PlanetAPIService planetService;

	@GetMapping(value = "/all/{page}")
	@ResponseStatus(HttpStatus.OK)
	public List<PlanetClientDTO> findAll(@PathVariable("page") int page) {
		
		PlanetResponse response = planetService.findAllPlanets(page);
		
		return convertToDTO(response.getResults(), PlanetClientDTO.class);
	}

}

