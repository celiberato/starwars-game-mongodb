package br.com.celiberato.starwars.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import br.com.celiberato.starwars.constants.MessagesConstants;
import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;
import br.com.celiberato.starwars.exception.BusinessException;
import br.com.celiberato.starwars.repository.PlanetRepository;
import br.com.celiberato.starwars.response.PlanetClientResponse;
import br.com.celiberato.starwars.response.PlanetResponse;
import br.com.celiberato.starwars.util.EntityDtoUtil;

@Service
public class PlanetDBService {

	@Autowired
	private PlanetRepository repository;

	@Autowired
	private PlanetAPIService apiService;

	public List<Planet> getAll() {
		return this.repository.findAll();
	}

	public Planet getPlanetById(String id) {
		return this.repository.findById(id).orElseThrow(IllegalArgumentException::new);
	}

	public List<Planet> getPlanetByName(String name) {
		return this.repository.findByName(name);
	}

	public Planet createPlanet(Planet planet) throws BusinessException {
		validate(planet);

		planet.setQuantity(sumPlanetFilms(this.getAllPlanets(), planet.getName()));

		return repository.save(planet);

	}

	public Planet updatePlanet(String id, PlanetDto planetDto) {
		planetDto.setId(id);
		return this.repository.save(EntityDtoUtil.toEntity(planetDto));
	}

	public void deletePlanet(String id) throws BusinessException {
		if (!repository.existsById(id)) {
			throw new BusinessException(MessagesConstants.PLANET_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}
		this.repository.deleteById(id);
	}

	public void validate(Planet planet) throws BusinessException {

		if (!ObjectUtils.isEmpty(planet.getId()) && !repository.existsById(planet.getId())) {
			throw new BusinessException(MessagesConstants.PLANET_INEXISTENTE, HttpStatus.BAD_REQUEST);
		}

	}

	public Set<PlanetClientResponse> getAllPlanets() {
		Set<PlanetClientResponse> lista = new HashSet<>();
		PlanetResponse response = null;

		int page = 1;
		while (response == null || response.getNext() != null) {
			response = apiService.findAllPlanets(page++);
			lista.addAll(response.getResults());
		}
		return lista;
	}

	public Long sumPlanetFilms(Set<PlanetClientResponse> allPlanets, String planetName) {
		Set<PlanetClientResponse> samePlanet = allPlanets.stream().filter((obj -> obj.getName().equals(planetName)))
				.collect(Collectors.toSet());

		Long quantity = 0L;
		for (PlanetClientResponse PlanetClientResponse : samePlanet) {
			if (PlanetClientResponse.getName().equals(planetName)) {
				quantity += PlanetClientResponse.getFilms().size();
			}
		}

		return quantity;
	}

	public boolean existsById(String id) {
		return repository.findById(id).isPresent();
	}

	public void deleteById(String id) {
		repository.deleteById(id);
	}

}
