package br.com.celiberato.starwars.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.celiberato.starwars.client.StarWarsClient;
import br.com.celiberato.starwars.response.PlanetResponse;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@FieldDefaults(level = AccessLevel.PROTECTED)
@Service
public class PlanetAPIService {

    @Autowired
	private StarWarsClient starWarsClient;
  
    public PlanetResponse findAllPlanets(int page) {
    	return starWarsClient.findAllPlanets(page);
    }
}

