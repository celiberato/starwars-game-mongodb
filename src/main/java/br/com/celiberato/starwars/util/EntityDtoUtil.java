package br.com.celiberato.starwars.util;

import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

public class EntityDtoUtil {

    public static PlanetDto toDto(Planet planet){
        PlanetDto dto = new PlanetDto();
        BeanUtils.copyProperties(planet, dto);
        return dto;
    }

    public static Planet toEntity(PlanetDto planetDto){
        Planet planet = new Planet();
        BeanUtils.copyProperties(planetDto, planet);
        return planet;
    }

    public static List<PlanetDto> toDto(List<Planet> listaPlanet){
    	List<PlanetDto> resultado = new ArrayList<>();
    	
    	for(Planet planet: listaPlanet) {
            PlanetDto dto = new PlanetDto();
            BeanUtils.copyProperties(planet, dto);
    	
            resultado.add(dto);
    	}
    	
        return resultado;
    }

    public static List<Planet> toEntity(List<PlanetDto> listaPlanetDto){
    	List<Planet> resultado = new ArrayList<>();
    	
    	for(PlanetDto planetDto: listaPlanetDto) {
            Planet entity = new Planet();
            BeanUtils.copyProperties(planetDto, entity);
    	
            resultado.add(entity);
    	}
    	
        return resultado;
    }

}
