package br.com.celiberato.starwars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpringBootWebApplication {

	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/starwars");
		SpringApplication.run(SpringBootWebApplication.class, args);
	}

}
