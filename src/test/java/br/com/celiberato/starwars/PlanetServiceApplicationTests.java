package br.com.celiberato.starwars;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.reactive.function.BodyInserters;

import br.com.celiberato.starwars.dto.PlanetDto;
import br.com.celiberato.starwars.entity.Planet;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SpringBootWebApplication.class)
@WebAppConfiguration
class PlanetServiceApplicationTests extends AbstractTests{

	@Test
	void contextLoads() {
	}

	@Test
	public void createPlanetTest() throws URISyntaxException {
		
		PlanetDto planetDto = convertToDTO(
				builderPlanet(
					"Alderaan", 
					"temperate, tropical", 
					"jungle, rainforests"), PlanetDto.class);

		PlanetDto dto = userWebclient().post()
	            .uri("/db/planets/create")
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON)
	            .body(BodyInserters.fromValue(planetDto))
	            .retrieve()
	            .bodyToMono(PlanetDto.class)
	            .block();

		assertEquals(planetDto.getName(), dto.getName());
		assertEquals(planetDto.getClimate(), dto.getClimate());
		assertEquals(planetDto.getTerrain(), dto.getTerrain());
	}


	@Test
	void listAllPlanets() {
		// CRIA PLANETA...
		PlanetDto planetDto = convertToDTO(
				builderPlanet(
					"Geonosis", 
					"temperate, tropical", 
					"jungle, rainforests"), PlanetDto.class);

		PlanetDto dto = userWebclient().post()
	            .uri("/db/planets/create")
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON)
	            .body(BodyInserters.fromValue(planetDto))
	            .retrieve()
	            .bodyToMono(PlanetDto.class)
	            .block();

		assertEquals(planetDto.getName(), dto.getName());
		assertEquals(planetDto.getClimate(), dto.getClimate());
		assertEquals(planetDto.getTerrain(), dto.getTerrain());

		// RECUPERA PRIMEIRO PLANETA...
		PlanetDto dtoFirst = userWebclient().get()
	            .uri("/db/planets/all")
	            .retrieve()
	            .bodyToFlux(PlanetDto.class)
	            .blockFirst();
	    
	    assertNotNull(dtoFirst);
	}	

	@Test
	void listById() {
		// CRIA PLANETA...
		PlanetDto planetDto = convertToDTO(
				builderPlanet(
					"Geonosis", 
					"temperate, tropical", 
					"jungle, rainforests"), PlanetDto.class);

		PlanetDto dto = userWebclient().post()
	            .uri("/db/planets/create")
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON)
	            .body(BodyInserters.fromValue(planetDto))
	            .retrieve()
	            .bodyToMono(PlanetDto.class)
	            .block();

		assertEquals(planetDto.getName(), dto.getName());
		assertEquals(planetDto.getClimate(), dto.getClimate());
		assertEquals(planetDto.getTerrain(), dto.getTerrain());

		// RECUPERA PRIMEIRO PLANETA...
		PlanetDto dtoById = userWebclient().get()
	            .uri("/db/planets/" + dto.getId())
	            .retrieve()
	            .bodyToMono(PlanetDto.class)
	            .block();
	    
	    assertEquals(dtoById.getName(), dto.getName());
	}	

	@Test
	void listByName() {
		// CRIA PLANETA...
		PlanetDto planetDto = convertToDTO(
				builderPlanet(
					"Geonosis", 
					"temperate, tropical", 
					"jungle, rainforests"), PlanetDto.class);

		PlanetDto dto = userWebclient().post()
	            .uri("/db/planets/create")
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON)
	            .body(BodyInserters.fromValue(planetDto))
	            .retrieve()
	            .bodyToMono(PlanetDto.class)
	            .block();

		assertEquals(planetDto.getName(), dto.getName());
		assertEquals(planetDto.getClimate(), dto.getClimate());
		assertEquals(planetDto.getTerrain(), dto.getTerrain());

		// RECUPERA PRIMEIRO PLANETA...
		PlanetDto dtoByName = userWebclient().get()
	            .uri("/db/planets/name/" + dto.getName())
	            .retrieve()
	            .bodyToFlux(PlanetDto.class)
	            .blockFirst();
	    
	    assertEquals(dtoByName.getName(), dto.getName());
	}	
	
	@Test
	void listAllAPIPlanets() {
		// RECUPERA PRIMEIRO PLANETA...
		PlanetDto dtoFirst = userWebclient().get()
	            .uri("/api/planets/all/1")
	            .retrieve()
	            .bodyToFlux(PlanetDto.class)
	            .blockFirst();
	    
	    assertNotNull(dtoFirst);
	}	
	
	public Planet builderPlanet(
			String name, 
			String climate, 
			String terrain) {
		
		return Planet.builder()//
				.name(name)
				.climate(climate)
				.build();
	}


}
