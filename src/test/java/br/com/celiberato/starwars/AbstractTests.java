package br.com.celiberato.starwars;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

public abstract class AbstractTests {
	protected ModelMapper modelMapper =  new ModelMapper();

	public WebClient userWebclient() {
		return WebClient.builder().baseUrl("http://127.0.0.1:8091/starwars/").build();
	}

	
	protected <D, T> List<D> convertToDTO(final Iterable<T> models, final Class<D> dtoClass) {
		List<D> dtos = new ArrayList<>();
		for (T model : models) {
			dtos.add(modelMapper.map(model, dtoClass));
		}

		return dtos;
	}

	protected <D, T> List<D> convertToVO(final Iterable<T> models, final Class<D> voClass) {
		List<D> dtos = new ArrayList<>();
		for (T model : models) {
			dtos.add(modelMapper.map(model, voClass));
		}

		return dtos;
	}
	public <D, T> D convertToDTO(final T model, final Class<D> dtoClass) {
		return modelMapper.map(model, dtoClass);
	}

	public <V, T> V convertToVO(final T model, final Class<V> voClass) {
		return modelMapper.map(model, voClass);
	}
	
	public HttpHeaders getHeaders(final String token) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + token);

		return headers;
	}

}
