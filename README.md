Checkout do Game StarWars!
- https://gitlab.com/celiberato/starwars-game-mongodb.git

Como testar a API via POSTMAN?

Instale o software Postman:
- https://www.postman.com/downloads/

Importe o arquivo exportado do postman, no seguinte link:
- https://gitlab.com/celiberato/starwars-game-mongodb/-/blob/master/postman/starwars.postman_collection.json

Como executar teste untários:

Instale a última versão do eclipse spring tools suite 4
- https://spring.io/tools

Clone o projeto para uma pasta do disco
- https://gitlab.com/celiberato/starwars-game-mongodb.git


Execute o maven update do projeto

Suba o projeto na interface do eclipse (ATENÇÃO: deixe o projeto no ar antes de execuar os testes unitários)

Execute os testes unitários do junit (Run As -> JUnit Test)
